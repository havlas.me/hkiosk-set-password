hKiosk Set Password Service
===========================

[![MIT license][license-image]][license-link]

A simple systemd service that sets the password of the `hkiosk` user to the value defined by `SET_PASSWORD` variable in
the `/media/.set/password` file - that is automatically removed once the password is updated. Service is run only once
at boot time and can't be run manually. There is a cleanup service that removes the `/media/.set/password` file before
the system is shut down.

The password is updated using the `chpasswd` command.

Installation
------------

```shell
DESTDIR= make install
```

Environment Variables
---------------------

* **`SET_PASSWORD`**
  * The password to be set.
* **`USERNAME`**
  * The username of the user whose password is to be updated.
  * Default: `hkiosk`

Usage
-----

Create a `/media/.set/password` file containing the variable `SET_PASSWORD` set to the desired password.

```dotenv
SET_PASSWORD="my-new-password"
```

License
-------

[MIT][license-link]


[license-image]: https://img.shields.io/badge/license-MIT-blue.svg?style=flat-square
[license-link]: LICENSE
