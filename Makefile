.PHONY: install
install:
	install -d $(DESTDIR)/usr/lib/systemd/system
	install -m 644 systemd/hkiosk-set-password.service $(DESTDIR)/usr/lib/systemd/system/
	install -m 644 systemd/hkiosk-cleanup-password.service $(DESTDIR)/usr/lib/systemd/system/
	install -d $(DESTDIR)/usr/libexec
	install -m 755 libexec/hkiosk-set-password $(DESTDIR)/usr/libexec/

.PHONY: uninstall
uninstall:
	-rm $(DESTDIR)/usr/lib/systemd/system/hkiosk-set-password.service
	-rm $(DESTDIR)/usr/lib/systemd/system/hkiosk-cleanup-password.service
	-rm $(DESTDIR)/usr/libexec/hkiosk-set-password
